#!/bin/bash
#
# @@script: install-java.sh
# @@description: Quick install Java on ubuntu or debian
# @@author: Loouis Low
# @@copyright: GNU GPL
#

### runas root
function runas_root () {
   if [ "$(whoami &2>/dev/null)" != "root" ] && [ "$(id -un &2>/dev/null)" != "root" ]
      then
         echo "[jsone] Permission denied."
         exit 1
   fi
}

# No args
if [ $# -eq 0 ]
  then
    echo "Which one? : [debian|ubuntu]"
    exit 1
fi

# Too many args
if [ $# -ge 2 ]
  then
    echo "Which one? : [debian|ubuntu]]"
    exit 1
fi

# Debian
if [ "$1" == "debian" ]
then
	echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee /etc/apt/sources.list.d/webupd8team-java.list
	echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee -a /etc/apt/sources.list.d/webupd8team-java.list
	sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886
	sudo apt-get update
	sudo apt-get -y install oracle-java8-installer
fi

# Ubuntu
if [ "$1" == "ubuntu" ]
then
	sudo add-apt-repository ppa:webupd8team/java
	sudo apt-get update
	sudo apt-get -y install oracle-java8-installer
fi
