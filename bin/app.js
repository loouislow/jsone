(function(a) {
    if (typeof define === "function" && define.amd) {
        define(a)
    } else {
        if (typeof exports === "object") {
            module.exports = a()
        } else {
            var c = window.Cookies;
            var b = window.Cookies = a();
            b.noConflict = function() {
                window.Cookies = c;
                return b
            }
        }
    }
}(function() {
    function b() {
        var f = 0;
        var c = {};
        for (; f < arguments.length; f++) {
            var d = arguments[f];
            for (var e in d) {
                c[e] = d[e]
            }
        }
        return c
    }

    function a(d) {
        function c(o, n, k) {
            var r;
            if (arguments.length > 1) {
                k = b({
                    path: "/"
                }, c.defaults, k);
                if (typeof k.expires === "number") {
                    var h = new Date();
                    h.setMilliseconds(h.getMilliseconds() + k.expires * 86400000);
                    k.expires = h
                }
                try {
                    r = JSON.stringify(n);
                    if (/^[\{\[]/.test(r)) {
                        n = r
                    }
                } catch (m) {}
                if (!d.write) {
                    n = encodeURIComponent(String(n)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent)
                } else {
                    n = d.write(n, o)
                }
                o = encodeURIComponent(String(o));
                o = o.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
                o = o.replace(/[\(\)]/g, escape);
                return (document.cookie = [o, "=", n, k.expires && "; expires=" + k.expires.toUTCString(), k.path && "; path=" + k.path, k.domain && "; domain=" + k.domain, k.secure ? "; secure" : ""].join(""))
            }
            if (!o) {
                r = {}
            }
            var q = document.cookie ? document.cookie.split("; ") : [];
            var p = /(%[0-9A-Z]{2})+/g;
            var l = 0;
            for (; l < q.length; l++) {
                var j = q[l].split("=");
                var f = j[0].replace(p, decodeURIComponent);
                var g = j.slice(1).join("=");
                if (g.charAt(0) === '"') {
                    g = g.slice(1, -1)
                }
                try {
                    g = d.read ? d.read(g, f) : d(g, f) || g.replace(p, decodeURIComponent);
                    if (this.json) {
                        try {
                            g = JSON.parse(g)
                        } catch (m) {}
                    }
                    if (o === f) {
                        r = g;
                        break
                    }
                    if (!o) {
                        r[f] = g
                    }
                } catch (m) {}
            }
            return r
        }
        c.get = c.set = c;
        c.getJSON = function() {
            return c.apply({
                json: true
            }, [].slice.call(arguments))
        };
        c.defaults = {};
        c.remove = function(f, e) {
            c(f, "", b(e, {
                expires: -1
            }))
        };
        c.withConverter = a;
        return c
    }
    return a(function() {})
}));

var Lang = (function() {
    var a = function() {
        this._fireEvents = true;
        this._dynamic = {}
    };
    a.prototype.init = function(d) {
        var b = this,
            f, c, g, e;
        d = d || {};
        d.cookie = d.cookie || {};
        c = d.defaultLang;
        g = d.currentLang;
        e = d.allowCookieOverride;
        this.cookieName = d.cookie.name || "langCookie";
        this.cookieExpiry = d.cookie.expiry || 365;
        this.cookiePath = d.cookie.path || "/";
        this._mutationCopies = {
            append: $.fn.append,
            appendTo: $.fn.appendTo,
            prepend: $.fn.prepend,
            before: $.fn.before,
            after: $.fn.after,
            html: $.fn.html
        };
        $.fn.append = function() {
            return b._mutation(this, "append", arguments)
        };
        $.fn.appendTo = function() {
            return b._mutation(this, "appendTo", arguments)
        };
        $.fn.prepend = function() {
            return b._mutation(this, "prepend", arguments)
        };
        $.fn.before = function() {
            return b._mutation(this, "before", arguments)
        };
        $.fn.after = function() {
            return b._mutation(this, "after", arguments)
        };
        $.fn.html = function() {
            return b._mutation(this, "html", arguments)
        };
        this.defaultLang = c || "en";
        this.currentLang = c || "en";
        if ((e || !g) && typeof Cookies !== "undefined") {
            f = Cookies.get(this.cookieName);
            if (f) {
                g = f
            }
        }
        $(function() {
            b._start();
            if (g && g !== b.defaultLang) {
                b.change(g)
            }
        })
    };
    a.prototype.pack = {};
    a.prototype.attrList = ["title", "alt", "placeholder", "href"];
    a.prototype.dynamic = function(c, b) {
        if (c !== undefined && b !== undefined) {
            this._dynamic[c] = b
        }
    };
    a.prototype.loadPack = function(c, d) {
        var b = this;
        if (c && b._dynamic[c]) {
            $.ajax({
                dataType: "json",
                url: b._dynamic[c],
                success: function(h) {
                    b.pack[c] = h;
                    if (b.pack[c].regex) {
                        var g = b.pack[c].regex,
                            f, e;
                        for (e = 0; e < g.length; e++) {
                            f = g[e];
                            if (f.length === 2) {
                                f[0] = new RegExp(f[0])
                            } else {
                                if (f.length === 3) {
                                    f[0] = new RegExp(f[0], f[1]);
                                    f.splice(1, 1)
                                }
                            }
                        }
                    }
                    if (d) {
                        d(false, c, b._dynamic[c])
                    }
                },
                error: function() {
                    if (d) {
                        d(true, c, b._dynamic[c])
                    }
                    throw ("Error loading language pack" + b._dynamic[c])
                }
            })
        } else {
            throw ("Cannot load language pack, no file path specified!")
        }
    };
    a.prototype._start = function(c) {
        var b = c !== undefined ? $(c).find("[lang]") : $(":not(html)[lang]"),
            e = b.length,
            d;
        while (e--) {
            d = $(b[e]);
            this._processElement(d)
        }
    };
    a.prototype._processElement = function(b) {
        if (b.attr("lang") === this.defaultLang) {
            this._storeAttribs(b);
            this._storeContent(b)
        }
    };
    a.prototype._storeAttribs = function(e) {
        var d, b, c;
        for (d = 0; d < this.attrList.length; d++) {
            b = this.attrList[d];
            if (e.attr(b)) {
                c = e.data("lang-attr") || {};
                c[b] = e.attr(b);
                e.data("lang-attr", c)
            }
        }
    };
    a.prototype._storeContent = function(c) {
        if (c.is("input")) {
            switch (c.attr("type")) {
                case "button":
                case "submit":
                case "hidden":
                case "reset":
                    c.data("lang-val", c.val());
                    break
            }
        } else {
            if (c.is("img")) {
                c.data("lang-src", c.attr("src"))
            } else {
                var b = this._getTextNodes(c);
                if (b) {
                    c.data("lang-text", b)
                }
            }
        }
    };
    a.prototype._getTextNodes = function(e) {
        var b = e.contents(),
            c = [],
            f = {},
            h, d = this,
            g = Array.prototype.map;
        $.each(b, function(i, j) {
            if (j.nodeType !== 3) {
                return
            }
            f = {
                node: j,
                langDefaultText: j.data
            };
            c.push(f)
        });
        if (b.length == 1) {
            c[0].langToken = e.data("langToken")
        }
        return c
    };
    a.prototype._setTextNodes = function(f, c, d) {
        var h, g, j, b, k = d !== this.defaultLang;
        for (h = 0; h < c.length; h++) {
            g = c[h];
            if (k) {
                j = g.langToken || $.trim(g.langDefaultText);
                if (j) {
                    b = this.translate(j, d);
                    if (b) {
                        try {
                            g.node.data = g.node.data.split($.trim(g.node.data)).join(b)
                        } catch (i) {}
                    } else {
                        if (console && console.log) {
                            console.log('Translation for "' + j + '" not found!')
                        }
                    }
                }
            } else {
                try {
                    g.node.data = g.langDefaultText
                } catch (i) {}
            }
        }
    };
    a.prototype._translateAttribs = function(d, e) {
        var b, c = d.data("lang-attr") || {},
            f;
        for (b in c) {
            if (c.hasOwnProperty(b)) {
                if (d.attr(b)) {
                    if (e !== this.defaultLang) {
                        f = this.translate(c[b], e);
                        if (f) {
                            d.attr(b, f)
                        }
                    } else {
                        d.attr(b, c[b])
                    }
                }
            }
        }
    };
    a.prototype._translateContent = function(d, e) {
        var b = e !== this.defaultLang,
            f, c;
        if (d.is("input")) {
            switch (d.attr("type")) {
                case "button":
                case "submit":
                case "hidden":
                case "reset":
                    if (b) {
                        f = this.translate(d.data("lang-val"), e);
                        if (f) {
                            d.val(f)
                        }
                    } else {
                        d.val(d.data("lang-val"))
                    }
                    break
            }
        } else {
            if (d.is("img")) {
                if (b) {
                    f = this.translate(d.data("lang-src"), e);
                    if (f) {
                        d.attr("src", f)
                    }
                } else {
                    d.attr("src", d.data("lang-src"))
                }
            } else {
                c = d.data("lang-text");
                if (c) {
                    this._setTextNodes(d, c, e)
                }
            }
        }
    };
    a.prototype.change = function(b, d, i) {
        var j = this;
        if (b === this.defaultLang || this.pack[b] || this._dynamic[b]) {
            if (b !== this.defaultLang) {
                if (!this.pack[b] && this._dynamic[b]) {
                    this.loadPack(b, function(l, k, m) {
                        if (!l) {
                            j.change.call(j, b, d, i)
                        } else {
                            if (i) {
                                i("Language pack could not load from: " + m, b, d)
                            }
                        }
                    });
                    return
                } else {
                    if (!this.pack[b] && !this._dynamic[b]) {
                        if (i) {
                            i("Language pack not defined for: " + b, b, d)
                        }
                        throw ("Could not change language to " + b + " because no language pack for this language exists!")
                    }
                }
            }
            var f = false,
                e = this.currentLang;
            if (this.currentLang != b) {
                this.beforeUpdate(e, b);
                f = true
            }
            this.currentLang = b;
            var g = d !== undefined ? $(d).find("[lang]") : $(":not(html)[lang]"),
                h = g.length,
                c;
            while (h--) {
                c = $(g[h]);
                if (c.attr("lang") !== b) {
                    this._translateElement(c, b)
                }
            }
            if (f) {
                this.afterUpdate(e, b)
            }
            if (typeof Cookies !== "undefined") {
                Cookies.set(j.cookieName, b, {
                    expires: j.cookieExpiry,
                    path: j.cookiePath
                })
            }
            if (i) {
                i(false, b, d)
            }
        } else {
            if (i) {
                i("No language pack defined for: " + b, b, d)
            }
            throw ('Attempt to change language to "' + b + '" but no language pack for that language is loaded!')
        }
    };
    a.prototype._translateElement = function(b, c) {
        this._translateAttribs(b, c);
        if (b.attr("data-lang-content") != "false") {
            this._translateContent(b, c)
        }
        b.attr("lang", c)
    };
    a.prototype.translate = function(c, b) {
        b = b || this.currentLang;
        if (this.pack[b]) {
            var d = "";
            if (b != this.defaultLang) {
                d = this.pack[b].token[c];
                if (!d) {
                    d = this._regexMatch(c, b)
                }
                if (!d) {
                    if (console && console.log) {
                        console.log('Translation for "' + c + '" not found in language pack: ' + b)
                    }
                }
                return d || c
            } else {
                return c
            }
        } else {
            return c
        }
    };
    a.prototype._regexMatch = function(h, g) {
        var c, i, f, e, d, b;
        c = this.pack[g].regex;
        if (c) {
            i = c.length;
            for (f = 0; f < i; f++) {
                e = c[f];
                d = e[0];
                b = d.exec(h);
                if (b && b[0]) {
                    return h.split(b[0]).join(e[1])
                }
            }
        }
        return ""
    };
    a.prototype.beforeUpdate = function(c, b) {
        if (this._fireEvents) {
            $(this).triggerHandler("beforeUpdate", [c, b, this.pack[c], this.pack[b]])
        }
    };
    a.prototype.afterUpdate = function(c, b) {
        if (this._fireEvents) {
            $(this).triggerHandler("afterUpdate", [c, b, this.pack[c], this.pack[b]])
        }
    };
    a.prototype.refresh = function() {
        this._fireEvents = false;
        this.change(this.currentLang);
        this._fireEvents = true
    };
    a.prototype._mutation = function(f, g, e) {
        var b = this._mutationCopies[g].apply(f, e),
            d = this.currentLang,
            c = $(f);
        if (c.attr("lang")) {
            this._fireEvents = false;
            this._translateElement(c, this.defaultLang);
            this.change(this.defaultLang, c);
            this.currentLang = d;
            this._processElement(c);
            this._translateElement(c, this.currentLang)
        }
        this._start(c);
        this.change(this.currentLang, c);
        this._fireEvents = true;
        return b
    };
    return a
})();
