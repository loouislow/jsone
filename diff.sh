#!/bin/bash
#
# @@script: diff.sh
# @@description: Example diff settings for server
# @@author: Loouis Low
# @@copyright: GNU GPL
#

### WRITE YOUR NEW FUNCTION AT BELOW
# FOR DIFFERENT OUTPUT DIRECTIVES.

### runas root
function runas_root () {
   if [ "$(whoami &2>/dev/null)" != "root" ] && [ "$(id -un &2>/dev/null)" != "root" ]
      then
         echo "[jsone] Permission denied."
         exit 1
   fi
}

### seek diff - vendor
function seek_diff_vendor () {
   
   OUTPUT_DIR="output"
   LATEST_DIR="latest"
   OLD_DIR="old"
   VENDOR_NEW=${OUTPUT_DIR}/${LATEST_DIR}/vendor.min.js
   VENDOR_OLD=${OUTPUT_DIR}/${OLD_DIR}/vendor.min.js

   if ! diff "$VENDOR_OLD" "$VENDOR_NEW" >/dev/null 2>&1;
      then
         echo "[jsone] File $VENDOR_NEW has modified" >&2
         
         if [ -f "$VENDOR_NEW" ]
            then
               echo "[jsone] Updating/Overwrite file $VENDOR_OLD ..."
               
               rm -rf $VENDOR_OLD
               cp $VENDOR_NEW $VENDOR_OLD
            else
               echo "[jsone] File $VENDOR_NEW existed!"
         fi
         
         echo "[jsone] Folder $VENDOR_OLD Updated!"
     else
         echo "[jsone] Folder $VENDOR_OLD is identical! Nothing to update."
   fi
}

### initialize
#runas_root
seek_diff_vendor
