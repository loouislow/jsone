#!/bin/bash
#
# @@script: build.sh
# @@description: Example build settings
# @@author: Loouis Low
# @@copyright: GNU GPL
#

### include settings
source jsone.conf

### runas root
function runas_root () {
   if [ "$(whoami &2>/dev/null)" != "root" ] && [ "$(id -un &2>/dev/null)" != "root" ]
      then
         echo "[jsone] Permission denied."
         exit 1
   fi
}

### builder
function start_everything () {

   # check prerequisite
   if which java node >/dev/null;
       then

       # delete previous package
       rm -rf $p

       # everything was fine
       echo "js: [jsone] Working..."

       java -jar $l/$b $t1/$s1/$all > $p

       #java -jar $l/$b $t1/$s1/$j1 $t1/$s1/$j2 $t1/$s1/$j3 $t1/$s1/$j4 $t1/$s1/$j5 $t1/$s1/$j6 > $p

       #java -jar $l/$b $t1/$s1/$all $t1/$s2/$all $t1/$s3/$all $t1/$s4/$all > $p

       # check package health
       mapfile -n 10 < $p
       if ((${#MAPFILE[@]}>1));
       then
           echo "js: [jsone] Successful. Package file name: $p"
       else
           # delete corrupted package
           rm -rf $p
           echo "js: [jsone] $p package is corrupted. Deleted. Halt!"
       fi

   else
       # install prerequisites
       echo "js: [jsone] Oracle JDK 9-ea required. Installing..."
       
       bash $l/install-java.sh ${linux}
   fi
   
}

### initialize
start_everything

