# jsone (free)

JavaScript minifier, merger and a smart compiler

Build: 1.5461 (free), java version "9-ea"

> Smart JS rewrite algorithms, make your code cleaner, lighter, smarter and more efficient to render.

### Supported

###### free build

- Generic JavaScript
- Bootstrap Material Design
- Google Material Design
- Materialize
- Material-UI
- Ionic Material
- jQuery

###### licensed build

- Angular.Js
- App.Js
- Aurelia.js
- Backbone.js
- Electron
- Ember.Js
- Enyo.Js
- Ext.Js
- Knockout.js
- Meteor.Js
- Mercury.js
- Node.js
- Polymer.js
- React.Js
- Tabris.Js
- Vue.js

### Prerequisites

You need these packages to be installed.

- Oracle JRE/JDK

The `build.sh` script will automatically add new repo and install it for you if no JDK existed.

```
./install-java.sh
```

### File structure

```txt
jsone/
   | install-java.sh -- (install prerequisites)
   | build.sh -- (editable convenient package builder)
   | diff.sh -- (automation script for cron)
   | jsone.conf -- (customize your settings here)
   | README.md -- (you are reading at me!)

     bin/
       | js.jar -- (core library)
       | jsone-arch64.jar -- (compiler, that's me!)

     files/
       | vendor -- (put js framework/library here)
       | scripts -- (put js apps here)
       | app -- (put js app here)
       | init -- (put js function here)

     output/
       | old -- (direct your project here)
       | new -- (new build is done by diff.sh)
```

inside the `jsone.conf` file, you can customize it if you saw fit,

```bash
#!/bin/bash

#############################
# jsone config bash script
#############################

# jar
l=bin/
b=jsone-arch64.jar

# target folder
t1=files

# structure folders
s1=vendor
s2=scripts
s3=app
s4=init

# js files
j1=jquery-1.11.1.js
#j2=jquery-migrate-1.2.1.js
#j3=jquery-ui-1.11.2.js
#j4=main-gsap.js
#j5=bootstrap.js
#j6=layout.js

# all files
all=*.js

# package name
p=output/latest/vendor.min.js

# jdk (debian or ubuntu)
linux=ubuntu
```

### Before Anything Else

Place your JS framework into "vendor" folder, JS plugins goes to `scripts`, JS App put into `app` and JS functions scripts is in the `init` folder if any.

1. vendor > js framework
2. scripts > js plugin/library
3. app > js app
4. init > js functions

### USAGE

Execute the commands at below in Terminal.

Quick package building, you will need to edit the `build.sh` script for different setup,

```
$ ./build.sh
```

To compile all files in a folder,

```
$ java -jar jsone-arch64.jar files/vendor/*.js > vendor.min.js
```

To compile specific files with priority, file1 is a first, file2 is a second,

```
$ java -jar jsone-arch64.jar file1.js file2.js > file.min.js
```

To compile multiple folders,

```
$ java -jar jsone-arch64.jar folder1/*.js folder2/*.js > file.min.js
```

To check build version or help,

```
$ java -jar jsone-arch64.jar -?
```

### For Server

jsone at the production web/app server, create a custom script to watchdog file changes and execute with cronjob, or create a custom script run in background as services.

Use the example diff.sh script to set up properly.

```
$ ./diff.sh
```
